//
//  TodoListCell.swift
//  TodoApp
//
//  Created by Michael Rausch on 21/06/18.
//  Copyright © 2018 Michael Rausch. All rights reserved.
//

import UIKit

class TodoListCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTitle(title: String){
        self.title.text = title
    }
    
    func setPurchased(isPurchased: Bool) {
        if !isPurchased {
            return
        }
        
        self.title.textColor = UIColor.green
    }
}
