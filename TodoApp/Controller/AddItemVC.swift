//
//  AddItemVC.swift
//  TodoApp
//
//  Created by Michael Rausch on 21/06/18.
//  Copyright © 2018 Michael Rausch. All rights reserved.
//

import UIKit
import CoreData

class AddItemVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var itemInput: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        itemInput.delegate = self
        itemInput.becomeFirstResponder()
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        itemInput.resignFirstResponder()
        dismiss(animated: true, completion: nil);
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let itemName = itemInput.text{
            if itemName == "" {
                return false
            }
            
            itemInput.resignFirstResponder()
            
            let item = ShoppingListItem(context: context)
            
            item.title = itemName
            item.purchased = false
            
            ad.saveContext()
            
        }
        
        dismiss(animated: true, completion: nil)
        return true
    }
    
}
