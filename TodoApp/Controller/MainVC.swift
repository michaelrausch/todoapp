//
//  MainVC.swift
//  TodoApp
//
//  Created by Michael Rausch on 21/06/18.
//  Copyright © 2018 Michael Rausch. All rights reserved.
//

import UIKit
import CoreData

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    
    @IBOutlet weak var todoListTable: UITableView!
    var fetchedResultsController: NSFetchedResultsController<ShoppingListItem>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        todoListTable.delegate = self
        todoListTable.dataSource = self
        setupFetchRequest()
        doFetchRequest()
    }
    
    func setupFetchRequest(){
        let request: NSFetchRequest<ShoppingListItem> = ShoppingListItem.fetchRequest()
        let titleSort = NSSortDescriptor(key: "title", ascending: true)
        
        request.sortDescriptors = [titleSort]
        
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        self.fetchedResultsController.delegate = self
    }
    
    func doFetchRequest(){
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            let error = error as NSError
            fatalError("Fetch Error \(error)")
        }
    }
    
    // MARK - Table methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController.sections {
            return sections[section].numberOfObjects
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = todoListTable.dequeueReusableCell(withIdentifier: "todoListCell") as! TodoListCell
        
        let item = fetchedResultsController.object(at: indexPath as IndexPath)
        
        if let title = item.title{
            cell.setTitle(title: title)
            cell.setPurchased(isPurchased: item.purchased)
        }
        
        return cell;
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        if let sections = self.fetchedResultsController.sections{
            return sections.count
        }
        
        return 0
    }
    
    func getItemByName(withName name: String) -> ShoppingListItem? {
        let request: NSFetchRequest<ShoppingListItem> = ShoppingListItem.fetchRequest()
        var results: [ShoppingListItem]
        request.predicate = NSPredicate(format: "title == '\(name)'")
        
        do {
            results = try context.fetch(request)
        } catch {
            return nil
        }
        
        if results.count > 0{
            return results[0]
        }
        
        return nil
    }
    
    func deleteItem(withName name: String) {
        let item = getItemByName(withName: name)
        
        if let item = item {
            context.delete(item)
            ad.saveContext()
        }
    }
    
    func setItemPurchased(withName name: String) {
        let item = getItemByName(withName: name)
        
        if let item = item {
            item.purchased = true
            ad.saveContext()
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = deleteItemAction(forRowAtIndexPath: indexPath)

        let config = UISwipeActionsConfiguration(actions: [delete])
        return config
    }
    
    func deleteItemAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {

        let action = UIContextualAction(style: .normal,
                                        title: "Done") { (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
                                            
                                            let item = self.todoListTable.cellForRow(at: indexPath) as! TodoListCell
                                            
                                            if let title = item.title.text {
                                                self.deleteItem(withName: title)
                                            }
                                            
                                            self.todoListTable.reloadData()
                                            
        }

        action.backgroundColor = UIColor.green
        return action
    }
    
    // MARK: Fetched results controller methods
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        todoListTable.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        todoListTable.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                todoListTable.insertRows(at: [indexPath], with: .fade)
            }
            
            break
            
        case .delete:
            if let indexPath = indexPath {
                todoListTable.deleteRows(at: [indexPath], with: .fade)
            }
            
            break
            
        case .update:
            if let indexPath = indexPath {
                let cell = todoListTable.cellForRow(at: indexPath) as! TodoListCell
                
                let item = fetchedResultsController.object(at: indexPath as IndexPath)
        
                if let title = item.title{
                    cell.setTitle(title: title)
                    cell.setPurchased(isPurchased: item.purchased)
                }
            }
            
            break
            
        case .move:
            if let indexPath = indexPath {
                todoListTable.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let indexPath = newIndexPath {
                todoListTable.insertRows(at: [indexPath], with: .fade)
            }
            
            break
        }
    
    }


}

